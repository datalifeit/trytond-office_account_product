# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.modules.company_office.model import (OfficeMultiValueMixin,
    OfficeValueMixin)
from trytond.pyson import Eval
from trytond.transaction import Transaction


class ProductCategory(OfficeMultiValueMixin, metaclass=PoolMeta):
    __name__ = 'product.category'

    def get_account(self, name, **pattern):
        pattern = pattern.copy()
        pattern.setdefault('office', Transaction().context.get('office'))
        return super().get_account(name, **pattern)


class ProductCategoryAccount(OfficeValueMixin, metaclass=PoolMeta):
    __name__ = 'product.category.account'

    @classmethod
    def __setup__(cls):
        cls._office_required = False
        super().__setup__()
        req_pyson = Eval('_parent_category', {}).get('accounting', False)
        if 'required' in cls.office.states:
            cls.office.states['required'] |= req_pyson
        else:
            cls.office.states['required'] = req_pyson
        cls.office.depends.append('category')
