datalife_office_account_product
===============================

The office_account_product module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-office_account_product/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-office_account_product)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
