# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import configuration
from . import product


def register():
    Pool.register(
        configuration.AccountConfiguration,
        configuration.ConfigurationDefaultAccount,
        product.ProductCategory,
        product.ProductCategoryAccount,
        module='office_account_product', type_='model')
