# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.modules.company_office.model import (OfficeMultiValueMixin,
    OfficeValueMixin)

__all__ = ['AccountConfiguration', 'ConfigurationDefaultAccount']


class AccountConfiguration(OfficeMultiValueMixin, metaclass=PoolMeta):
    __name__ = 'account.configuration'


class ConfigurationDefaultAccount(OfficeValueMixin, metaclass=PoolMeta):
    __name__ = 'account.configuration.default_account'

    @classmethod
    def __setup__(cls):
        cls._office_required = False
        super().__setup__()
